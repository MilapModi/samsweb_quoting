

const commonSteps = require('../pageobjects/commonSteps');
const testData = require('../pageobjects/testData');
const LoginPage = require('../pageobjects/login.page');
const quoting = require('../pageobjects/quoting.page');
const navigateToCreateQuote = require('../pageobjects/navigateToCreateQuote.page');
const insuredDetails = require('../pageobjects/insuredDEtails.page');
const billingInfo = require('../pageobjects/billingInfo.page');
const mortgageInfo = require('../pageobjects/mortgageInfo.page');
const summary = require('../pageobjects/summary.page');
const policy = require('../pageobjects/policy.page');

describe('creates Quote and Generates Policy in MSWeb', () => {

  it('should verify Title of Login Page', async () => {

    browser.url(testData.url);
    browser.maximizeWindow();
    expect(browser).toHaveTitle('Login to TypTap Insurance Portal');
  })

  it('should login with valid credentials', async () => {

    await LoginPage.login(testData.email, testData.pwd);
    await LoginPage.verifyLoginPageTitle();

  })

  it('navigate to Quoting Page', async () => {

    await navigateToCreateQuote.clickClientServicesBtn();
    await navigateToCreateQuote.clickQuoteCreateBtn();
    await navigateToCreateQuote.clickPropertyAddSearchBox();
    await navigateToCreateQuote.setValuePropertyAddSearchBox(testData.testAddress);
    await navigateToCreateQuote.clickPropertyAddSearchBtn();
    await navigateToCreateQuote.clickPropertyAddress();
    await commonSteps.completeStep("Navigation to Quoting Page");

  })

  it('should fill out Quoting information & start application', async () => {

    await quoting.clickMonthsLivedBtn();
    await quoting.setValueMonthsLived(testData.monthsLived);
    await quoting.clickPriorFloodClaims();
    await quoting.setValuePriorFloodClaims(testData.priorFloodClaims);
    await quoting.clickInsuredFirstNameBtn();
    await quoting.setValueInsuredFirstName(testData.insuredFirstName);
    await quoting.clickInsuredLastNameBtn();
    await quoting.setValueInsuredLastName(testData.insuredLasttName);
    await quoting.clickInsuredEmailBox();
    await quoting.setValueInsuredEmail(testData.insuredEmail);
    await quoting.clickRateBtn();
    await quoting.clickSaveBtn();
    await quoting.clickStartAppBtn();
    var quoteNumber =  await quoting.getQuoteNumber();
    await console.log("Quote number is =>     " + quoteNumber );
    await browser.reportMessage("Quote number is =>     " + quoteNumber);
    await commonSteps.completeStep("Application Started");

  })

  it('should fill out Insured Details Tab', async () => {

    await insuredDetails.clickInsuredDetailsTab();
    await insuredDetails.clickinsuredPrimaryPhone();
    await insuredDetails.setValueInsuredPrimaryPhone(testData.insuredPrimaryPhone);
    await insuredDetails.clickSameAddressSelect();
    await insuredDetails.clickSaveBtn();
    await commonSteps.completeStep("Insured Page");

  })

  it('should fill out Mortgage Information Tab', async () => {

    await mortgageInfo.clickMortgageInfoTab();
    await mortgageInfo.setValueFirstMortgageCompany(testData.firstMortgageCompanyName);
    await mortgageInfo.clickAddressMortgageCompany();
    await mortgageInfo.setValueAddressMortgageCompany(testData.addressMortgageCompany);
    await mortgageInfo.clickCityMortgageCompany();
    await mortgageInfo.setValueCityMortgageCompany(testData.cityMortgageCompany);
    await mortgageInfo.clickStateMortgageCompany();
    await mortgageInfo.setValueStateMortgageCompany(testData.stateMortgageCompany);
    await mortgageInfo.clickZipMortgageCompany();
    await mortgageInfo.setValueZipMortgageCompany(testData.zipMortgageCompany);
    await mortgageInfo.clickSaveBtn();
    await commonSteps.completeStep("Mortgage Information Tab");

  })

  it('should select billing information', async () => {

    await billingInfo.clickBillingInfoTab();
    await billingInfo.clickBillTo();
    await billingInfo.setValueBillTo(testData.billTo);
    await billingInfo.clickAnnualBillPlan();
    await billingInfo.clickSaveBtn();
    await commonSteps.completeStep("Billing Information");

  })

  it('should navigate to summary page and click on Application Complete', async () => {

    await summary.clickSummaryTab();
    await summary.clickApplicationCompleteBtn();
    await browser.getAlertText();
    await browser.acceptAlert();
    await commonSteps.completeStep("Application Completed");

  })

  it('should bind the Quote and Generate the Policy', async () => {

    await policy.clickBindPolicyBtn();
    await browser.acceptAlert();
    var policyNumber = await policy.getPolicyNumber();
    await console.log("Policy number is =>       " + policyNumber);
    browser.reportMessage("Policy number is =>       " + policyNumber)
    await commonSteps.completeStep("Policy Created");
  })

})

//run command
//npm run test:uat
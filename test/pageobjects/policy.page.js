const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class policy extends Page {

    ////////////// Page Object Locators////////

    get bindPolicyBtn() { return $('#cphMaster_tbconQuote_tbpnlReview_lbBindPolicy') }
    get policyNumber() { return $('//*[@id="tbPolicyHolder"]/tbody/tr[1]/td[2]') }



    ////////////////// page functions ////////////////////////////

    async clickBindPolicyBtn() {
        await commonSteps.doClick(await this.bindPolicyBtn)

    }


    async getPolicyNumber() {
        return await commonSteps.doGetText(await this.policyNumber)

    }

}

module.exports = new policy();
class commonSteps {

    async completeStep(message) {
        await console.log("============================================================================");
        await console.log(`TEST STEP COMPLETE : ${message}`);
        await console.log("============================================================================\n");

    }

    async failedStep(message) {
        await console.log("============================================================================");
        await console.log(`TEST STEP Failed : ${message}`);
        await console.log("============================================================================\n");

    }
    
    async doClick(element) {
        //await element.waitForDisplayed()
        await element.waitForExist({ timeout: 5000 })
        await element.click()
    }

    async doSetValue(element, value) {

        await element.waitForDisplayed()
        await element.setValue(value)
    }

    async doSelectByVisibleText(element, value) {

        await element.waitForDisplayed()
        await element.selectByVisibleText(value)
    }

    async doGetText(element) {
        await element.waitForDisplayed()
        return await element.getText()
    }
}
module.exports = new commonSteps()



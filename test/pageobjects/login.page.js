const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');


class LoginPage extends Page {

  ////////////////// Login Page Elements ////////////////////////////////

  get inputUsername() { return $('#cphSAMSMaster_lgnLogin_UserName') }
  get inputPassword() { return $('#cphSAMSMaster_lgnLogin_Password') }
  get btnSubmit() { return $('#cphSAMSMaster_lgnLogin_LoginButton') }


  /////////////// Page Constants////////////////
  loginPageTitle = "Welcome to TypTap Insurance";


  /////////////// Login Page Functions //////////////////////////


  async login(username, password) {
    await (await this.inputUsername).setValue(username);
    await (await this.inputPassword).setValue(password);
    await (await this.btnSubmit).click();
  }

  async verifyLoginPageTitle() {

    if (await browser.getTitle() === this.loginPageTitle) {
      await commonSteps.completeStep("Login Successful")
    }
    else {
      await commonSteps.failedStep("Login Failed")
    }

  }
}

module.exports = new LoginPage();
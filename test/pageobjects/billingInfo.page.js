const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class billingInfo extends Page {

    ////////////// Page Object Locators////////


    get billingTab() { return $('#__tab_cphMaster_cphQuote_tbconQuote_tbpnlBilling') }
    get billTo() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlBilling_ddlBillTo') }
    get saveBtn() { return $('#cphMaster_cphQuote_btnSave') }
    get annualBillPlan() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlBilling_rblBillingPlan_0') }



    ////////////////// page functions ////////////////////////////


    async clickBillingInfoTab() {
        await commonSteps.doClick(await this.billingTab)
    }
    async clickBillTo() {
        await commonSteps.doClick(await this.billTo)
    }
    async setValueBillTo(BillTo) {

        await commonSteps.doSelectByVisibleText(await this.billTo, BillTo)
    }
    async clickAnnualBillPlan() {
        await commonSteps.doClick(await this.annualBillPlan)
    }
    async clickSaveBtn() {
        await commonSteps.doClick(await this.saveBtn)
    }

}


module.exports = new billingInfo();


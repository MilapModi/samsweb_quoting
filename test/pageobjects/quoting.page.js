
const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class quoting extends Page {


    ////////////////// Login Page Elements ////////////////////////////////

    get monthsLived() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlQuoteInfo_ddlMonthsOccupied') }
    get priorFloodClaims() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlQuoteInfo_ddlKnownClaims') }
    get insuredFirstName() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlQuoteInfo_txtFirstName') }
    get insuredLastName() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlQuoteInfo_txtLastName') }
    get insuredEmail() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlQuoteInfo_txtEmail') }
    get rateBtn() { return $('#cphMaster_cphQuote_btnRate') }
    get saveBtn() { return $('#cphMaster_cphQuote_btnSave') }
    get startAppBtn() { return $('#cphMaster_cphQuote_btnStartApp') }
    get quoteNumber() { return $('#cphMaster_lblQuoteNumber') }


    /////////////// Page Constants////////////////
    loginPageTitle = "Welcome to TypTap Insurance";


    /////////////// Login Page Functions //////////////////////////
    async clickMonthsLivedBtn() {

        await commonSteps.doClick(await this.monthsLived)

    }
    async setValueMonthsLived(months) {

        await commonSteps.doSelectByVisibleText(await this.monthsLived, months)
    }

    async clickPriorFloodClaims() {

        await commonSteps.doClick(await this.priorFloodClaims)

    }
    async setValuePriorFloodClaims(claims) {

        await commonSteps.doSelectByVisibleText(await this.priorFloodClaims, claims)
    }

    async clickInsuredFirstNameBtn() {

        await commonSteps.doClick(await this.insuredFirstName)

    }
    async setValueInsuredFirstName(firstName) {

        await commonSteps.doSetValue(await this.insuredFirstName, firstName)
    }
    async clickInsuredLastNameBtn() {

        await commonSteps.doClick(await this.insuredLastName)

    }
    async setValueInsuredLastName(lastName) {
        await commonSteps.doSetValue(await this.insuredLastName, lastName)
    }

    async clickInsuredEmailBox() {

        await commonSteps.doClick(await this.insuredEmail)

    }
    async setValueInsuredEmail(email) {
        await commonSteps.doSetValue(await this.insuredEmail, email)

    }
    async clickRateBtn() {
        await commonSteps.doClick(await this.rateBtn)

    }

    async clickSaveBtn() {
        await commonSteps.doClick(await this.saveBtn)

    }

    async clickStartAppBtn() {
        await commonSteps.doClick(await this.startAppBtn)

    }

    async getQuoteNumber() {
        return await commonSteps.doGetText(await this.quoteNumber)

    }
}

module.exports = new quoting();
const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class mortgageInfo extends Page {

    ////////////// Page Elements/////////////////////////////

    get mortgageInfoTab() { return $('#__tab_cphMaster_cphQuote_tbconQuote_tbpnlAIInfo') }
    get firstMortgageCompanyName() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlAIInfo_txtMortgageCompanyName') }
    get addressMortgageCompany() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlAIInfo_txtMortgageAddress1') }
    get cityMortgageCompany() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlAIInfo_txtMortgageCity') }
    get stateMortgageCompany() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlAIInfo_txtMortgageState') }
    get zipMortgageCompany() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlAIInfo_txtMortgageZipCode') }
    get saveBtn() { return $('#cphMaster_cphQuote_btnSave') }



    ///////////////// Page Functions////////////////////
    async clickMortgageInfoTab() {
        await commonSteps.doClick(await this.mortgageInfoTab)
    }

    async setValueFirstMortgageCompany(firstMortgageCompany) {
        await commonSteps.doSetValue(await this.firstMortgageCompanyName, firstMortgageCompany)

    }

    async clickAddressMortgageCompany() {
        await commonSteps.doClick(await this.addressMortgageCompany)
    }

    async setValueAddressMortgageCompany(addressMortgageCompany) {
        await commonSteps.doSetValue(await this.addressMortgageCompany, addressMortgageCompany)

    }

    async clickCityMortgageCompany() {
        await commonSteps.doClick(await this.cityMortgageCompany)
    }

    async setValueCityMortgageCompany(CityMortgageCompany) {
        await commonSteps.doSetValue(await this.cityMortgageCompany, CityMortgageCompany)

    }

    async clickStateMortgageCompany() {
        await commonSteps.doClick(await this.stateMortgageCompany)
    }

    async setValueStateMortgageCompany(StateMortgageCompany) {
        await commonSteps.doSetValue(await this.stateMortgageCompany, StateMortgageCompany)
    }

    async clickZipMortgageCompany() {
        await commonSteps.doClick(await this.zipMortgageCompany)
    }

    async setValueZipMortgageCompany(ZipMortgageCompany) {
        await commonSteps.doSetValue(await this.zipMortgageCompany, ZipMortgageCompany)
    }

    async clickSaveBtn() {
        await commonSteps.doClick(await this.saveBtn)
    }

}
module.exports = new mortgageInfo();


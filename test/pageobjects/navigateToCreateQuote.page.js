const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class navigateToCreateQuote extends Page {

   //////// Element Locators////////

   get clientServicesBtn() { return $('#ASPxMenuSAMS_DXI1_PImg') }
   get quoteCreateBtn() { return $('#ASPxMenuSAMS_DXI1i4_T') }
   get propertyAddSearchBox() { return $('#cphMaster_txtPropAddress') }
   get propertyAddSearchBtn() { return $('#cphMaster_btnSearch') }
   get propertyAddress() { return $('//*[@id="cphMaster_gvQuoteSearch"]/tbody/tr[2]/td[1]/a') }



   //////////////// Page Actions ///////////////

   async clickClientServicesBtn() {

      await commonSteps.doClick(await this.clientServicesBtn)
   }

   async clickQuoteCreateBtn() {

      await commonSteps.doClick(await this.quoteCreateBtn)
   }

   async clickPropertyAddSearchBox() {

      await commonSteps.doClick(await this.propertyAddSearchBox)
   }

   async setValuePropertyAddSearchBox(testAddress) {

      await commonSteps.doSetValue(await this.propertyAddSearchBox, testAddress)
   }

   async clickPropertyAddSearchBtn() {
      await commonSteps.doClick(await this.propertyAddSearchBtn)
   }

   async clickPropertyAddress() {

      await commonSteps.doClick(await this.propertyAddress)
   }

}

module.exports = new navigateToCreateQuote();



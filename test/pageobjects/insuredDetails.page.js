const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');


class insuredDetails extends Page {

    ////////////////// Login Page Elements ////////////////////////////////
    get insuredDetailsTab() { return $('#__tab_cphMaster_cphQuote_tbconQuote_tbpnlInsuredDetails') }
    get insuredPrimaryPhone() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlInsuredDetails_txtPhone') }
    get sameAddressSelect() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlInsuredDetails_cbAddressSame') }
    get saveBtn() { return $('#cphMaster_cphQuote_btnSave') }


    /////////////// Page Constants////////////////
    loginPageTitle = "Welcome to TypTap Insurance";


    /////////////// Login Page Functions //////////////////////////
    async clickInsuredDetailsTab() {
        await commonSteps.doClick(await this.insuredDetailsTab)
    }

    async clickinsuredPrimaryPhone() {
        await commonSteps.doClick(await this.insuredPrimaryPhone)
    }

    async setValueInsuredPrimaryPhone(insuredPrimaryPhone) {

        await commonSteps.doSetValue(await this.insuredPrimaryPhone, insuredPrimaryPhone)
    }

    async clickSameAddressSelect() {
        await commonSteps.doClick(await this.sameAddressSelect)
    }

    async clickSaveBtn() {
        await commonSteps.doClick(await this.saveBtn)
    }

}
module.exports = new insuredDetails();
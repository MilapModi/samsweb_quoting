const Page = require('./page');
const commonSteps = require('../pageobjects/commonSteps');
const testData = require('./testData');

class summary extends Page {

    ////////////// Page Object Locators////////


    get summaryTab() { return $('#__tab_cphMaster_cphQuote_tbconQuote_tbpnlSummary') }
    get applicationCompleteBtn() { return $('#cphMaster_cphQuote_tbconQuote_tbpnlSummary_btnAppComplete') }


    ////////////////// page functions ////////////////////////////


    async clickSummaryTab() {
        await commonSteps.doClick(await this.summaryTab)
    }


    async clickApplicationCompleteBtn() {
        await commonSteps.doClick(await this.applicationCompleteBtn)
    }

}

module.exports = new summary();
